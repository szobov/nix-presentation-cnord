{ pkgs ? import <nixpkgs>{} } :

let
  stdenv = pkgs.stdenv;
  pythonPackages = pkgs.python27Packages;
in

let
empy = pythonPackages.buildPythonPackage rec {
      name = "empy-3.3.3";
      src = pkgs.fetchurl {
          url = "http://www.alcyone.com/software/empy/empy-latest.tar.gz";
          sha256 = "1cgikljjcqxgz168prpvb0bnirbdxf9wmgj0vlzzhzzwf4a229li"; };
      doCheck = false;
};
in

stdenv.mkDerivation rec {
  name = "px4";
  src = null;
  propagatedBuildInputs = [
       empy
       pkgs.glibc
       pkgs.ninja
       pkgs.gcc-arm-embedded-7
       pkgs.genromfs
       pythonPackages.pyyaml
       pythonPackages.pyserial
       pythonPackages.numpy
       pythonPackages.numpy-stl
       pythonPackages.toml
       pythonPackages.jinja2
  ];
}
