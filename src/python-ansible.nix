{ pkgs ? import <nixpkgs>{} } :

let

pyulog = pkgs.python36Packages.buildPythonPackage rec {
      pname = "pyulog";
      version = "0.6.0";
      src = pkgs.python36Packages.fetchPypi {
       inherit pname version;
       sha256 = "1n4ny5wjwcqiy9dsv5p87l16b8fpw9iflcwlfmqii7rxd96im5q4";
      };

      propagatedBuildInputs = [pkgs.python36Packages.numpy];
      doCheck = false;
};

in

pkgs.stdenv.mkDerivation rec {
  name = "albatross";
  src = null;

  propagatedBuildInputs = [
    pkgs.ansible
    pyulog
  ];
}
