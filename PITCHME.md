# Let's make life easier

---
## Python, ansible

---?code=src/python-ansible.nix&lang=nix

---
## gcc-arm-embedded

---?code=src/gcc-arm.nix&lang=nix

---
## C++, python2.7, genromfs

---?code=src/python-cpp-genrom.nix&lang=nix

---
## Ruby, Jekyll, bundix

[bundix](https://github.com/manveru/bundix)

---?code=src/blog.nix&lang=nix

---
# Thanks!
